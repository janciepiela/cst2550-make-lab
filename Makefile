CXX = g++
CXXFLAGS = -g -E -Wall - Wextra -Wpedantic

num_conv: numberconversion.cpp numberconversion.h
	$(CXX) $(CXXFLAGS) -o num_conv

roman: romandigitconverter.cpp
	$(CXX) $(CXXFLAGS) -o roman

numberconversion.o: numberconversion.cpp
	$(CXX) $(CXXFLAGS) -c numberconversion.cpp

.PHONY: clean
clean:
	$(RM) *.o num_conv

